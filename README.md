# Android Basic

Buku Android Basic ini adalah buku pertama dari Siswa SMK Rabbaanii kelas 10B-RPL, buku ini membahas tentang dasar-dasar  Android yang dikhususkan untuk pemula, buku ini juga dilengkapi dengan gambar sebagai pedoman untuk mengikuti dasar Android Ini dengan tahap demi tahap

Pembahasan akan difokuskan dibeberap materi yaitu:
1. [**Pengenalan Android**](http://gitlab.com/ProgrammerMuda/Android-Basic/tree/master/Pengenalan%20Android)

2. [**Pengenalan Dasar Android**](http://gitlab.com/ProgrammerMuda/Android-Basic/tree/master/Pengenalan%20Dasar%20Android)
3. [**Widget-widget yang ada diAndroid Studio**](http://gitlab.com/ProgrammerMuda/Android-Basic/tree/master/Widget-widget%20didalam%20Android)