# Pengenalan Dasar Android

Pada Pengenalan dasar Android ini kita akan membahas tentang Android yang mencakup beberapa materi yaitu
1. LifeCycler Android
2. Instalasi Android Studio
3. Requirement System
4. SDK manager di Android Studio
5. Membuat Project aplikasi pertama
6. Membuat AVD (Android Virtual Device)
7. Menjalankan Aplikasi Android di AVD atau Emulator
8. Layouting
